package org.example.atividade_9;

import java.util.Scanner;

//9) Leia o nome e a idade de 10 pessoas e exiba o nome da pessoa mais nova.
public class NomeIdade {
    public static void main(String[] args) {
        Scanner pessoa = new Scanner(System.in);
        String nome[] = new String[10];
        String nomeMenor;

        int i = 0;
        int x = 0;
        int menor=0;
        int idade[];

        for(idade = new int[10]; i < 10 && x < 10; x++) {
            System.out.println("Digite o nome: ");
            nome[i] = pessoa.next();
            i++;
            System.out.println("Digite a idade; ");
            idade[x] = pessoa.nextInt();
        }

        menor = idade[0];
        nomeMenor = nome[0];
        x = 0;

        for(i = 0; i < 10; i++) {
            if (idade[i] < menor) {
                menor = idade[i];
                nomeMenor = nome[x];
            }

            x++;
        }

        System.out.println("A pessoa mais nova é: " + nomeMenor + " com " + menor + " anos de idade");
    }

}
