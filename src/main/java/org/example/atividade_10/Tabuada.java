package org.example.atividade_10;

import java.util.Scanner;

//10) Crie um algoritmo leia um número do usuário e exiba a sua tabuada de
//multiplicação.
public class Tabuada {
    public static void main(String[] args) {
        Scanner digitar = new Scanner(System.in);
        int vet[] = new int[10];
        int x = 0;
        int mult = 1;
        int num = 0;
        System.out.println("Digite um número:");

        for(num = digitar.nextInt(); mult <= 10; mult++) {
            vet[x] = num * mult;
            x++;
        }

        for(x = 0; x <= 10; x++) {
            System.out.println("Resultado: " + vet[x]);
        }

    }
}
