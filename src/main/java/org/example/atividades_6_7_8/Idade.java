package org.example.atividades_6_7_8;

import java.util.Scanner;

//6) Leia a idade de 20 pessoas e exiba a soma das idades.
//7) Leia a idade de 20 pessoas e exiba a média das idades.
//8) Leia a idade de 20 pessoas e exiba quantas pessoas são maiores de idade.
public class Idade {
    public static void main(String[] args) {
        Scanner digitar = new Scanner(System.in);
        int idade[] = new int[20];
        int soma = 0;
        int cont = 0;
        int i;

        for(i = 0; i < 20; i++) {
            System.out.println("Digite a idade: ");
            idade[i] = digitar.nextInt();
            soma += idade[i];
            if (idade[i] >= 18) {
                cont++;
            }
        }

        System.out.println("Soma: " + soma);
        System.out.println("Média das idades: " + soma / i);
        System.out.println("Quantidade de pessoas maiores de idade: " + cont);
    }

}
