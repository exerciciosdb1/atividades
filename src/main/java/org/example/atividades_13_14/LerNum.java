package org.example.atividades_13_14;

import java.util.Scanner;

//13) Faça um algoritmo que leia 20 números e, ao final, escreva quantos estão entre 0 e
//100.
//14) Faça um algoritmo que leia 20 números e, ao final, escreva quantos estão entre 0 e
//100, quantos estão entre 101 e 200 e quantos são maiores de 200.
public class LerNum {
    public static void main(String[] args) {
        Scanner digitar = new Scanner(System.in);
        int num[] = new int[20];
        int x = 0;
        int numEntre = 0;
        int contEntre = 0;

        int contMaior;
        for(contMaior = 0; x < 20; x++) {
            System.out.println("Digite os números");
            num[x] = digitar.nextInt();
        }

        for(x = 0; x < 20; x++) {
            if (num[x] >= 0 && num[x] <= 100) {
                ++numEntre;
            } else if (num[x] >= 101 && num[x] <= 200) {
                contEntre++;
            } else if (num[x] > 200) {
                contMaior++;
            }
        }

        System.out.println("" + numEntre + " números estão entre 0 e 100");
        System.out.println("" + contEntre + " números estão entre 101 e 200");
        System.out.println("" + contMaior + " números são maiores que 200");
    }

}
