package org.example.atividade_15;

import java.util.Scanner;

//15) Escreva um algoritmo que leia uma sequência de números do usuário e realize a
//soma desses números. Encerre a execução quando um número negativo for digitado.
public class Soma {
    public static void main(String[] args) {
        Scanner digitar = new Scanner(System.in);
        int num = 0;
        int soma = 0;

        while(num >= 0) {
            System.out.println("Digite os números");
            num = digitar.nextInt();
            if (num >= 0) {
                soma += num;
            }
        }

        System.out.println("A soma dos números digitados são: " + soma);
    }
}
