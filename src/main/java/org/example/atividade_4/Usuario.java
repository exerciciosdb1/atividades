package org.example.atividade_4;

import java.util.Scanner;

//4) Leia o nome um número do usuário um número N e escreva o nome dele na tela N
//vezes.

public class Usuario {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String nome;
        int num=0;
        int vezes=0;
        int i=0;

        System.out.println("Digite o nome: ");
        nome = entrada.next();

        System.out.println("Digite o número: ");
        num = entrada.nextInt();

        System.out.println("Quantas vezes quer repetir? ");
        vezes = entrada.nextInt();

        for(i = 0; i < vezes; i++) {
            System.out.println("Nome: " + nome + " - Número: " + num);
        }

    }
}
