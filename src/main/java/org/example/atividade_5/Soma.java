package org.example.atividade_5;

import java.util.Scanner;

//5) Escreva um algoritmo que leia 10 números do usuário e calcule a soma desses
//números.
public class Soma {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int n = 10;
        int soma = 0;
        int num[] = new int[n];
        int i;

        for(i = 0; i < n; i++) {
            System.out.println("Digite um número");
            num[i] = ler.nextInt();
        }

        for(i = 0; i < n; i++) {
            soma += num[i];
        }

        for(i = 0; i < n; i++) {
            System.out.print(num[i] + " ");
        }

        System.out.println("");
        System.out.println("A soma é: " + soma);
    }
}
