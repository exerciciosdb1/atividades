package org.example.atividade_2;

//2) Escreva um algoritmo que calcule a soma dos números de 1 a 15.
public class Calcular {
    public static void main(String[] args) {
        int soma = 0;
        int i=0;

        for(i = 1; i <= 15; i++) {
            soma += i;
        }

        System.out.println("A soma é " + soma);
    }
}
