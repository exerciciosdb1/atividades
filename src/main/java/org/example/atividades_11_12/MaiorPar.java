package org.example.atividades_11_12;

import java.util.Scanner;

//11) Escreva um algoritmo que leia 20 números do usuário e exiba quantos números são
//maiores do que 8.
//12) Escreva um algoritmo que leia 20 números do usuário e exiba quantos números são
//pares
public class MaiorPar {
    public static void main(String[] args) {
        Scanner digitar = new Scanner(System.in);
        int[] num = new int[20];
        int x = 0;
        int cont = 0;

        int contPar;
        for(contPar = 0; x < 20; ++x) {
            System.out.println("Digite os números:");
            num[x] = digitar.nextInt();
        }

        for(x = 0; x < 20; ++x) {
            if (num[x] > 8) {
                ++cont;
            }
        }

        for(x = 0; x < 20; ++x) {
            if (num[x] % 2 == 0) {
                ++contPar;
            }
        }

        System.out.println("" + cont + " números são maiores que 8");
        System.out.println("" + contPar + " números são pares");
    }

}
